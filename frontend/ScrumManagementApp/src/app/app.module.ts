import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReleasesComponent } from './dashboard/releases/releases.component';
import { ViewmembersComponent } from './dashboard/viewmembers/viewmembers.component';
import { HeaderComponent } from './dashboard/header/header.component';
import { AddreleaseComponent } from './dashboard/addrelease/addrelease.component';
import { AuthComponent } from './auth/auth.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SettingsComponent } from './dashboard/settings/settings.component'



@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ReleasesComponent,
    ViewmembersComponent,
    HeaderComponent,
    AddreleaseComponent,
    AuthComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
