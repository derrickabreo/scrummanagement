import { Component, OnInit } from '@angular/core';
import { Release } from './release.model';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Task } from './task.model';

@Component({
  selector: 'app-releases',
  templateUrl: './releases.component.html',
  styleUrls: ['./releases.component.scss']
})
export class ReleasesComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient) { }

  ngOnInit() {
    this.getReleases();
  }

  releases: Release[];
  tasks: Task[];
  RELEASES_URL = "http://localhost:8082/releases";
  TASKS_URL = "http://localhost:8082/tasks/";

  getReleases() {
    this.httpClient.get<Release[]>(this.RELEASES_URL).subscribe(
      releases => {
        this.releases = releases;
        this.getTasksForRelease();
      });
  }

  getTasksForRelease(){
    this.releases.forEach(release => {
      console.log(release.tableid);
      this.httpClient.get<Task[]>(this.TASKS_URL+release.tableid).subscribe(
        tasks => {
          release.tasks = tasks;
          console.log("tasks");
          console.log(release.tasks)
        });
    });
  }

  showTaskDetails(jira_no: number){
    console.log(jira_no);
  }

  

}
