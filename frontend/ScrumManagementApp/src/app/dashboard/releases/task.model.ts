export class Task {
    jira_no: number;
    task_name: string;
    owner: string;
    uat_date: Date;
    status: string;
    tableid: number;
    updateStatus: string;
  }
  