import { Task } from './task.model';

export class Release {
    tableid: number;
    date: string;
    anchor: number;
    status: string;
    tasks: Task[];
  }
  