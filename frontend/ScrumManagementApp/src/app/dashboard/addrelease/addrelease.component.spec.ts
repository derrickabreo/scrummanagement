import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddreleaseComponent } from './addrelease.component';

describe('AddreleaseComponent', () => {
  let component: AddreleaseComponent;
  let fixture: ComponentFixture<AddreleaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddreleaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddreleaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
