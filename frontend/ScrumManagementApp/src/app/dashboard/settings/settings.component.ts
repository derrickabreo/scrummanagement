import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient) { }

  ngOnInit() {
    this.error = false;
  }

  error: boolean;

  NAME_URL = "http://localhost:8082/changeName";
  PASSWORD_URL = "http://localhost:8082/changePassword";

  onSubmitPass(form: NgForm){
        
    form.value.soeid = localStorage.getItem("soeid");
    console.log(form.value);

    if(form.value.con_password == form.value.new_password){
      this.httpClient.post<any>(this.PASSWORD_URL, form.value).subscribe(
        response => {
          console.log(response);
        },
        (err) => console.log(err)
      );
      this.error = false;
    } else {
      this.error = true;
    }

    
    
    form.reset();
    
  }

  onSubmitName(form: NgForm){

    form.value.soeid = localStorage.getItem("soeid");
    
    this.httpClient.post<any>(this.NAME_URL, form.value).subscribe(
      response => {
        console.log(response);
      },
      (err) => console.log(err)
    );
    
    
    form.reset();
    
  }

}
