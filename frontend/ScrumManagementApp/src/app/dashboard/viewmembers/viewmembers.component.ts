import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Member } from './member.model';
import { Toggle } from './toggle.model';

@Component({
  selector: 'app-viewmembers',
  templateUrl: './viewmembers.component.html',
  styleUrls: ['./viewmembers.component.scss']
})
export class ViewmembersComponent implements OnInit {
  togglesm: Toggle;

  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient) { }

  ngOnInit() {
    this.getMembers();
  }

  members: Member[];
  ADD_URL = "http://localhost:8082/addmember";
  MEMBERS_URL = "http://localhost:8082/members";
  TOGGLESM_URL = "http://localhost:8082/toggleSMRole";
  REMOVE_URL = "http://localhost:8082/deletemember/";

  getMembers() {
    this.httpClient.get<Member[]>(this.MEMBERS_URL).subscribe(
      members => {
        this.members = members as Member[];
        console.log("members");
        console.log(members);

        console.log("this.members");
        console.log(this.members);
      });

      
  }

  AddMember(form: NgForm){
    
    console.log(form.value);

    this.httpClient.post<any>(this.ADD_URL, form.value).subscribe(
      response => {
        console.log(response);
        this.getMembers();
      },
      (err) => console.log(err)
    );
    
    
    
    form.reset();
    
  }

  toggleSMRights(soeid: number, currentAccess: boolean){

    this.togglesm = new Toggle();
    this.togglesm.ssm = currentAccess;
    this.togglesm.soeid = soeid;
    

    this.httpClient.post<any>(this.TOGGLESM_URL, this.togglesm).subscribe(
      response => {
        console.log(response);
        this.getMembers();
      },
      (err) => console.log(err)
    );
  }

  removeMember(soeid: number){ 
    console.log(soeid);
    this.httpClient.delete(this.REMOVE_URL+soeid).subscribe(res =>{
      this.getMembers();
    });
  }

}
