import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})

export class AuthComponent implements OnInit {
   constructor(private formBuilder: FormBuilder, private httpClient: HttpClient, private router: Router) { }

    AUTH_URL = "http://localhost:8082/login";

    ngOnInit() {
        if(localStorage.getItem("status") == "true"){
            console.log("Redirect");
            this.router.navigate(['/dashboard']);
        }
    }

    onSubmit(form: NgForm){
        
        console.log(form.value);
        
        this.httpClient.post<any>(this.AUTH_URL, form.value).subscribe(
            response => {
                console.log(response);
                localStorage.setItem("status", response.loginStatus);
                localStorage.setItem("ssm", response.sm);
                localStorage.setItem("sm", response.ssm);
                localStorage.setItem("soeid", response.soeid);
                console.log(localStorage.getItem("status"));
                console.log(localStorage.getItem("ssm"));
                console.log(localStorage.getItem("sm"));
                console.log(localStorage.getItem("soeid"));

                if(localStorage.getItem("status") == "true"){
                    console.log("Redirect");
                    this.router.navigate(['/dashboard']);
                }
            },
            (err) => console.log(err)
        );
        
        form.reset();
        
    }
}