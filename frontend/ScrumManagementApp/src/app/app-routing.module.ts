import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthComponent } from './auth/auth.component';
import { ViewmembersComponent } from './dashboard/viewmembers/viewmembers.component';
import { AddreleaseComponent } from './dashboard/addrelease/addrelease.component';
import { SettingsComponent } from './dashboard/settings/settings.component';


const routes: Routes = [
  { path: '',               component: AuthComponent },
  { path: 'dashboard',      component: DashboardComponent },
  { path: 'members',        component: ViewmembersComponent },
  { path: 'settings',       component: SettingsComponent },
  { path: 'addRelease',     component: AddreleaseComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
