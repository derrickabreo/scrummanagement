DROP DATABASE scrumDB;

Create database scrumDB;
USE scrumDB;

CREATE TABLE users
(	
	SOEID int AUTO_INCREMENT Primary Key,
	name VARCHAR(20),
	email VARCHAR(60),
	password VARCHAR(20),
	role ENUM('Dev', 'QA', 'DBDev', 'manager', 'BA'),
	SecondaryScrum boolean,
	IsScrumMaster boolean
);


create TABLE Releases (
	tableid INT NOT NULL PRIMARY KEY auto_increment,
    r_date DATE NOT NULL,
    r_anchor varchar(100) NOT NULL,
    r_status enum( 'Requirement analysis', 'Feasibility/impact study','Development', 'Unit testing', 'Integration testing', 'UAT testing coordination', 'Deployment activity', 'post-release activity')
);


create TABLE tasks (
	JiraNumber INT NOT NULL PRIMARY KEY auto_increment,
    task_name varchar(40) NOT NULL,
    owner_name VARCHAR(20) NOT NULL,
    uat_date DATE NOT NULL,
    task_status enum('Requirement analysis', 'Feasibility/impact study','Development', 'Unit testing', 'Integration testing', 'UAT testing coordination', 'Deployment
activity', 'post-release activity'),
	r_id INT NOT NULL,
    update_status varchar(500),
    FOREIGN KEY(r_id) references Releases(tableid)
);


create TABLE Meeting(
    SOEID INT NOT NULL,
    attended Bool,
    name varchar(50),
    email varchar(50),
    r_id INT NOT NULL,
    FOREIGN KEY(SOEID) references users(SOEID) ON delete cascade
);

INSERT INTO users(name,email,password,role,SecondaryScrum,IsScrumMaster) VALUES ('Ross','ross.delaney23@gmail.com', 'password','manager', false, true);
INSERT INTO users(name,email,password,role,SecondaryScrum,IsScrumMaster) VALUES ('Rishita','rishi.yadav07@gmail.com', 'password','BA', true, false);
INSERT INTO users(name,email,password,role,SecondaryScrum,IsScrumMaster) VALUES ('Citi','projectciti2019@gmail.com', 'password','manager', false, false);
INSERT INTO users(name,email,password,role,SecondaryScrum,IsScrumMaster) VALUES ('Derrick','derrickabreo@gmail.com', 'password','Dev', false, false);


select * from users;

INSERT INTO Releases(r_date,r_anchor,r_status) VALUES ('2019-10-07','Ross','Requirement analysis');
INSERT INTO Releases(r_date,r_anchor,r_status) VALUES ('2019-10-08','Rishita','Feasibility/impact study');
INSERT INTO Releases(r_date,r_anchor,r_status) VALUES ('2019-10-08','Citi','Unit testing');
INSERT INTO Releases(r_date,r_anchor,r_status) VALUES ('2019-10-08','Derrick','Development');



select * from releases;

INSERT INTO tasks(task_name,owner_name,uat_date,task_status,r_id,update_status) VALUES ('Create','Ross','2019-10-11','Requirement analysis',1,' Discussion with business to be done to get the queries clarified');
INSERT INTO tasks(task_name,owner_name,uat_date,task_status,r_id,update_status) VALUES ('Delete','Rishita','2019-10-11','Development',1,'Serverside changes in progress');
INSERT INTO tasks(task_name,owner_name,uat_date,task_status,r_id,update_status) VALUES ('Edit','Citi','2019-10-11','Unit testing',2,'Testing Completed');
INSERT INTO tasks(task_name,owner_name,uat_date,task_status,r_id,update_status) VALUES ('Update','Derrick','2019-10-11','Unit testing',2,'Testing Completed');
INSERT INTO tasks(task_name,owner_name,uat_date,task_status,r_id,update_status) VALUES ('Update','Derrick','2019-10-11','Unit testing',3,'Testing Completed');

select * from tasks;

insert into Meeting(SOEID,attended,name,email,r_id) values(1,True,'Ross','ross.delaney23@gmail.com',1);
insert into Meeting(SOEID,attended,name,email,r_id) values(2,True,'Rishita','rishi.yadav07@gmail.com',1);
insert into Meeting(SOEID,attended,name,email,r_id) values(3,False,'Citi','projectciti2019@gmail.com',2);
insert into Meeting(SOEID,attended,name,email,r_id) values(4,False,'Derrick','derrickabreo@gmail.com',2);

select * from Meeting;