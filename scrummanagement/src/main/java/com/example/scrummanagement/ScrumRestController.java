package com.example.scrummanagement;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.scrummanagement.business.AddMember;
import com.example.scrummanagement.business.ChangePassword;
import com.example.scrummanagement.business.EmailService;
import com.example.scrummanagement.business.NewUser;
import com.example.scrummanagement.business.ReleaseStatus;
import com.example.scrummanagement.business.Releases;
import com.example.scrummanagement.business.Tasks;
import com.example.scrummanagement.business.UserRepository;
import com.example.scrummanagement.business.Users;
import com.example.scrummanagement.dao.LoginReturnStatus;
import com.example.scrummanagement.dao.ToggleSM;
import com.example.scrummanagement.dao.MySQLJDBCUsers;
import com.google.gson.Gson;

@RestController
//Allows requests to come from different domains (Stops chrome from throwing an error)
@CrossOrigin
public class ScrumRestController {
	
	//Send email
	@Autowired
	EmailService emailservice;

	//Email Sender	
	@RequestMapping(method=RequestMethod.GET, value="/dailyUpdateMail")
	@ResponseStatus(HttpStatus.OK)
	public String sendEmail() throws MessagingException {
			emailservice.sendEmail();
			return "Daily Update Sent";
	}

	//Save the uploaded file to this folder
    private static String UPLOADED_FOLDER = "C:\\";
    Gson gson = new Gson();
    

    @Autowired
	UserRepository operations;

    @GetMapping("/")
    public String test() {
    	return "<!DOCTYPE html>\r\n" + 
    			"<html xmlns:th=\"http://www.thymeleaf.org\">\r\n" + 
    			"<body>\r\n" + 
    			"\r\n" + 
    			"<h1>Upload File</h1>\r\n" + 
    			"\r\n" + 
    			"<form method=\"POST\" action=\"/upload\" enctype=\"multipart/form-data\">\r\n" + 
    			"    <input type=\"file\" name=\"file\" /><br/><br/>\r\n" + 
    			"    <input type=\"submit\" value=\"Submit\" />\r\n" + 
    			"</form>\r\n" + 
    			"\r\n" + 
    			"</body>\r\n" + 
    			"</html>";
    }
    
    //Upload File 
    //TODO pass Release ID when uploading the file
    @PostMapping(value="/upload", consumes="multipart/form-data")
    public String singleFileUpload(@RequestParam String release_date, @RequestParam String release_anchor, @RequestParam("file") MultipartFile textfile,
                                   RedirectAttributes redirectAttributes) {
    	
    	System.out.println(release_date);
    	String[] data = null;
    	Date sqlDate = null;
    	
        if(textfile.isEmpty()){
        	redirectAttributes.addFlashAttribute("message", "File is Empty. Please select a file to upload");
        }

        try{
           // Get the file and save it in Tasks table
           byte[] bytes = textfile.getBytes();
           Path path = Paths.get(UPLOADED_FOLDER + textfile.getOriginalFilename());
           Files.write(path, bytes);

           BufferedReader br = new BufferedReader(new FileReader("C:\\release.txt"));
           String line;
   		
			while ((line  = br.readLine()) != null) {
				data = line.split(",");
				System.out.println(line);
				// Task task = new Task(data[0],data[1],data[2],data[3]);
			    System.out.println("1: "+ data[0] + " 2: " + data[1]+ " 3: "+ data[2] + " 4: " + data[3]+ " 4: " + data[4]);
			    sqlDate = parseDate(data[2]);
			    //TODO release ID currently set to 1
			    //Tasks(Jira,task name ,owner,date ,task status,Release ID,update status)
		        Tasks task = new Tasks(1,data[0], data[1],sqlDate,data[3],Integer.parseInt(data[4]),null );
				operations.addTasks(task);
			}
           br.close();
			
           redirectAttributes.addFlashAttribute("message", "You successfully uploaded '" + textfile.getOriginalFilename() + "'");
           
        } 
        catch (IOException e) {
        	e.printStackTrace();
        }      
        
        return "Success";
    }


	// TODO Login Methods  
    @RequestMapping(method=RequestMethod.POST, value="/login", consumes="application/json")
    public LoginReturnStatus logIn(@RequestBody LogInDetails details) {
    	String user = details.getEmail();
		String password = details.getPassword();
		System.out.println(user);
		
		LoginReturnStatus status =  operations.userLogin(user, password);
		
		return status;
    }
    
	//List all the releases
    @RequestMapping(method=RequestMethod.GET, value="/releases")
	public List<Releases> getReleases() {
    	// get list of team members from JDBC conn
    	// convert to JSON for response
		return operations.allReleases();
	}
	
    //Get all teamMemebers
	@RequestMapping(method=RequestMethod.GET, value="/members", produces="application/json")
	public List<Users> getTeamMembers() {
		// get list of team members from JDBC conn
		// convert to JSON for response
		return operations.allUsers();
	}
	
	//Get all the tasks
	@RequestMapping(method=RequestMethod.GET, value="/tasks/{id}")
	public List<Tasks> getTasks(@PathVariable("id") String id) {
		// get list of team members from JDBC conn
		// convert to JSON for response
		return operations.allTasks(Integer.parseInt(id));
	}
	
	//Is it working?
	//Toggle secondary scrum master role
	@RequestMapping(method=RequestMethod.POST, value="/toggleSMRole", consumes="application/json")
	public String changeSMRole(@RequestBody ToggleSM toggle) {
		// connect to database to update SM role
		// return confirmation of change.
		System.out.println(toggle.getSoeid());
		System.out.println(toggle.getSsm());
		return operations.changeSMRole(toggle);
	}
	
	//Delete a member
	@RequestMapping(method=RequestMethod.DELETE, value="/deletemember/{id}")
	public int deleteMember(@PathVariable("id") String id) {
		// get member to delete using id 
		// call to JDBC delete operation
		return operations.deleteMember(Integer.parseInt(id));
	}
	
	
	//Adds a new member
	@RequestMapping(method=RequestMethod.POST, value="/addmember")
	@ResponseStatus(HttpStatus.CREATED)
	public Users addTeamMember(@RequestBody AddMember member) {
		Users user = new Users(1, member.getName(), member.getEmail(), "password", member.getRole(), false , false);
		
		System.out.println(member.getName());
		System.out.println(member.getEmail());
		System.out.println(member.getRole());
		
		return operations.newMember(user);
	}
	
	
	//Daily updates to be stored in tasks table
	//TODO: Easier if only the SM or SSM could update the status
	@RequestMapping(method=RequestMethod.POST, value="/updateSpace/{jira}")
	public String updateSpace(@PathVariable("jira") String jira, String updatestatus) {
		int jiranumber =Integer.parseInt(jira);
		//String updatestatus = "New requirements Specs";
		return operations.updateSpace(updatestatus, jiranumber);
	}

	
	//Changing the Task status
	@RequestMapping(method=RequestMethod.POST, value="/updateTaskStatus/{jira}")
	public String updatetTaskStatus(@PathVariable("jira") String jira, String taskstatus) {
		int jiranumber =Integer.parseInt(jira);
		//String taskstatus = "Feasibility/impact study";
		return operations.updateTaskStatus(taskstatus, jiranumber);
	}
	
	//Changing the Release status
	@RequestMapping(method=RequestMethod.GET, value="/updateReleaseStatus")
	public ReleaseStatus updatetReleaseStatus() {//Feasibility/impact study
		ReleaseStatus taskStatusUpdate = new ReleaseStatus(4, "Feasibility/impact study");
		return operations.updateReleaseStatus(taskStatusUpdate);
	}
	
	
	/* 
	@RequestMapping(method=RequestMethod.POST, value="/dailyUpdate")
	public String sendDailyUpdate(@RequestBody DailyUpdate dailyUpdate) {
		// get all details of update
		// append them to email format
		// get all team members emails
		// send off email with details to all team members
		return "Daily Update Sent";
	}
	*/
	
	//TODO Is it working?
	//Change User password
	@RequestMapping(method=RequestMethod.POST, value="/changePassword")
	public String changePassword(@RequestBody ChangePassword changepassword) {
		return operations.changePassword(changepassword);
	}
	
	
	@RequestMapping(method=RequestMethod.POST, value="/changeName")
	public NewUser changeName(@RequestBody NewUser newName) {
		//NewUser newName = new NewUser(3, "New Joe");
		return operations.changeName(newName);
	}
	
	
	
	//Add new release
	@RequestMapping(method=RequestMethod.POST, value="/newRelease")
	public Releases newRelease(@RequestBody Releases newRelease) {//
		//Releases newRelease = new Releases(1, parseDate("2019-12-22"), 1, "Development");
		return operations.newReleases(newRelease);
	}
	
	
	
	//Convert string to SQL date format
	private static Date parseDate(String date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		java.sql.Date sqlDate = null;
        try {
            java.util.Date utilDate = format.parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
            System.out.println(sqlDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return sqlDate;
	}

}
