package com.example.scrummanagement.business;

public class AddMember {
	private int soeid;
	private String name;
	private String email;
	private String role;
	
	public AddMember(String name, String email, String role) {
		super();
		this.name = name;
		this.email = email;
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getSoeid() {
		return soeid;
	}

	public void setSoeid(int soeid) {
		this.soeid = soeid;
	}
}
