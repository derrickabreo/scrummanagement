package com.example.scrummanagement.business;

public class Meeting {
	private int SOEID;
	private boolean attended;
	private String name;
	private String email;
	private int RID;
	
	public Meeting(int SOEID, boolean attended, String name, String email, int RID) {
		super();
		this.SOEID = SOEID;
		this.attended = attended;
		this.name = name;
		this.email = email;
		this.RID = RID;
	}

	public int getSOEID() {
		return SOEID;
	}

	public void setSOEID(int SOEID) {
		this.SOEID = SOEID;
	}

	public boolean getAttended() {
		return attended;
	}

	public void setAttended(Boolean attended) {
		this.attended = attended;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getRID() {
		return RID;
	}

	public void setRID(int RID) {
		this.RID = RID;
	}
}
