package com.example.scrummanagement.business;

import java.sql.Date;

public class Releases {
	private int tableid;
	private Date date;
	private String anchor;
	private String status;
	
	public Releases(int tableid, Date date, String anchor, String status) {
		super();
		this.tableid = tableid;
		this.date = date;
		this.anchor = anchor;
		this.status = status;
	}

	
	public int getTableid() {
		return tableid;
	}

	public void setTableid(int tableid) {
		this.tableid = tableid;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAnchor() {
		return anchor;
	}

	public void setAnchor(String anchor) {
		this.anchor = anchor;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
