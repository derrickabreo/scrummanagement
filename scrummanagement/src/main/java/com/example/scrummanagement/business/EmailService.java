package com.example.scrummanagement.business;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import java.util.List;
import com.example.scrummanagement.dao.Data;

import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;

@Component
public class EmailService {

	@Autowired
    private JavaMailSender javaMailSender;
	
	@Autowired
	private Data repository;
	
	public void sendEmail() throws MessagingException {
		String emailString = "";
		String Attendees = "";
		String NonAttendees  = "";
		List<String> emailIDs = new ArrayList();
		Integer ReleaseID = null;
		 
		MimeMessage message = javaMailSender.createMimeMessage();
	    MimeMessageHelper messagehelper = new MimeMessageHelper(message, false);
	 
	    //Get date 
	    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	    Date date = new Date();
	    
        List<Releases> releases = repository.allReleases();
        List<Meeting> meetings = repository.meeting();
       
   //     for(Releases aRelease: releases) {
        	
        	for (Meeting meeting :meetings) {
        		emailIDs.add(meeting.getEmail()); 
        		if(meeting.getAttended()) {
        			Attendees = meeting.getName() + " " + Attendees;
        			//Attendees.add(meeting.getName());
        		}
        		else
        			//NonAttendees.add(meeting.getName());
        			 NonAttendees = meeting.getName() + " " + NonAttendees;
        	}
        //}
        
        for(Releases aRelease: releases) {
        	List<Tasks> tasks = repository.allTasks(aRelease.getTableid());
        	
        	//html_code = html_code+"Release ID:"+aRelease.getTableid();
        	
        	for(Tasks task: tasks) {
        		// ReleaseID = task.getTableid();
        		 emailString += "<tr><td>" + task.getJira_no() + "<td>" +
        				 task.getTask_name() + "<td>" + task.getOwner() + "<td>" +
        				 task.getStatus() + ": " + task.getUpdateStatus();
        	}
        	
        }
    
      
        
        for(int i=0;i<emailIDs.size();i++) {
        messagehelper.setTo(emailIDs.get(i));
       // System.out.println("Email ID :" + emailIDs.get(i));
        messagehelper.setSubject("Daily Update");
        messagehelper.setText("<!DOCTYPE html>" + 
        		"<html>" + 
        		"<head>" + 
        		"<style>" + 
        		"table {" + 
        		"  font-family: arial, sans-serif;" + 
        		"  border-collapse: collapse;" + 
        		"  width: 100%;" + 
        		"}" + 
        		"" + 
        		"td, th {" + 
        		"  border: 1px solid #dddddd;" + 
        		"  text-align: left;" + 
        		"  padding: 8px;" + 
        		"}" + 
        		"" + 
        		"tr:nth-child(even) {" + 
        		"  background-color: #dddddd;" + 
        		"}" + 
        		"</style>" + 
        		"</head>" + 
        		"<body>" + 
        		"<p>Attendees : " + Attendees + "</p>" +
        		"<p>Non-attendees: " + NonAttendees + "</p>" + 
        		"<p>Date : " + formatter.format(date)+"</p>" + 
        		"<p>Time Taken : 10 Mins </p><br>" +
        		"<p>Releases :</p>" + 
        		"<table>" + 
        		"  <tr>" + 
        		"    <th>JIRA number</th>" + 
        		"    <th>Name</th>" + 
        		"    <th>Owner</th>" + 
        		"    <th>Update</th>" + 
        		"  </tr>" + emailString +
        		"</table>" + 
        		"</body>", true);
  
        javaMailSender.send(message);
        }
        
    }
}
