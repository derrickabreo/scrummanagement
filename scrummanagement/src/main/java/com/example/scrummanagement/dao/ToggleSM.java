package com.example.scrummanagement.dao;

public class ToggleSM {
	private int soeid;
	private Boolean ssm;
	
	public ToggleSM(int soeid, Boolean ssm) {
		super();
		this.soeid = soeid;
		this.ssm = ssm;
	}
	public int getSoeid() {
		return soeid;
	}
	public void setSoeid(int soeid) {
		this.soeid = soeid;
	}
	public Boolean getSsm() {
		return ssm;
	}
	public void setSsm(Boolean ssm) {
		this.ssm = ssm;
	}
	
	

}
