package com.example.scrummanagement.dao;

public class LoginReturnStatus {
	boolean isSM;
	boolean isSSM;
	boolean loginStatus;
	int soeid;
	
	public LoginReturnStatus(boolean loginStatus, boolean isSM, boolean isSSM, int soeid) {
		super();
		this.isSM = isSM;
		this.isSSM = isSSM;
		this.soeid = soeid;
		this.loginStatus = loginStatus; 
	}

	public boolean isSM() {
		return isSM;
	}

	public boolean isLoginStatus() {
		return loginStatus;
	}

	public boolean isSSM() {
		return isSSM;
	}

	public int getSoeid() {
		return soeid;
	}

	 
}
