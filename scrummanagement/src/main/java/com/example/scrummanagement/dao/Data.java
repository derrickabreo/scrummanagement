package com.example.scrummanagement.dao;

import java.util.List;

import com.example.scrummanagement.business.AddMember;
import com.example.scrummanagement.business.ChangePassword;
import com.example.scrummanagement.business.Meeting;
import com.example.scrummanagement.business.NewUser;
import com.example.scrummanagement.business.ReleaseStatus;
import com.example.scrummanagement.business.Releases;
import com.example.scrummanagement.business.Tasks;
import com.example.scrummanagement.business.Users;

public interface Data {
	public List<Users> allUsers();
	public Tasks addTask(Tasks t);
	public String changePassowrd(ChangePassword changepassword);
	public LoginReturnStatus loginStatus(String email, String password);
	public List<Releases> allReleases();
	public List<Tasks> allTasks(int id);
	public String updateSpace(String update, int jira);
	public String updateTaskStatus(String taskstatus, int jira);
	public Users newMember(Users user);
	public int deleteMember(int soeid);
	public String changeSMRole(ToggleSM toogleScrumMaster);
	public Releases newRelease(Releases release);
	public ReleaseStatus updateReleaseStatus(ReleaseStatus taskStatusUpdate);
	public List<Meeting> meeting();
	public NewUser changeName(NewUser changeName);
}
