package com.example.scrummanagement.dao;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.config.Task;
import org.springframework.stereotype.Repository;

import com.example.scrummanagement.business.AddMember;
import com.example.scrummanagement.business.ChangePassword;
import com.example.scrummanagement.business.Meeting;
import com.example.scrummanagement.business.NewUser;
import com.example.scrummanagement.business.ReleaseStatus;
import com.example.scrummanagement.business.Releases;
import com.example.scrummanagement.business.Tasks;
import com.example.scrummanagement.business.Users;


@Repository
public class MySQLJDBCUsers implements Data {

	private static final Logger LOGGER = LoggerFactory.getLogger(MySQLJDBCUsers.class);
	
	private final String CONNECTION_URL = "jdbc:mysql://localhost:3306/ScrumDB?useSSL=false&serverTimezone=UTC" +
			"&allowPublicKeyRetrieval=true";
	//Lists all users
	@Override
	public List<Users> allUsers() {
		List<Users> users = new ArrayList<>();
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			Statement st = cn.createStatement();
			rs = st.executeQuery("SELECT * FROM users");
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				users.add(new Users(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getBoolean(6),rs.getBoolean(7)));
			}
		}
		catch(SQLException ex){
			LOGGER.error("Error Message: " + ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " + ex.getMessage());
				}
			}
		}
		return users;
	}
		
	//Returns login status along with SM and SSM status 
	@Override
	public LoginReturnStatus loginStatus(String email, String password) {
        boolean status = false;
        Connection cn = null;
		ResultSet rs = null;
		LoginReturnStatus login_status  = new LoginReturnStatus(false,false, false, 0);
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement("SELECT * FROM users WHERE email=? and password = ?");
			st.setString(1, email);
			st.setString(2, password);
			rs = st.executeQuery();
			status = rs.next();
			System.out.println(rs.getString("SOEID"));
			//Store the results in an object (Scrum Master, Secondary Scrum Master, Login status)
			login_status = new LoginReturnStatus(status,rs.getBoolean("IsScrumMaster"),rs.getBoolean("SecondaryScrum"),rs.getInt("SOEID"));
		}
		catch(SQLException ex){
			LOGGER.error("Error Message: " + ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " +ex.getMessage());
				}
			}
		}
	
		return login_status;
		 
	}

	//List All releases
	@Override
	public List<Releases> allReleases() {
		List<Releases> releases = new ArrayList<>();
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			Statement st = cn.createStatement();
			rs = st.executeQuery("SELECT * FROM releases");
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				releases.add(new Releases(rs.getInt(1),rs.getDate(2),rs.getString(3),rs.getString(4)));
			}
		}
		catch(SQLException ex){
			LOGGER.error("Error Message: " +ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " +ex.getMessage());
				}
			}
		}
		return releases;
	}

	//Lists All tasks based on the ID
	@Override
	public List<Tasks> allTasks(int id) {
		List<Tasks> tasks = new ArrayList<>();
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement("SELECT * FROM tasks WHERE r_id=?");
			st.setInt(1, id);
			rs = st.executeQuery();
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				tasks.add(new Tasks(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getDate(4),rs.getString(5),rs.getInt(6),rs.getString(7)));
			}
		}
		catch(SQLException ex){
			LOGGER.error("Error Message: " +ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " +ex.getMessage());
				}
			}
		}
		return tasks;
	}
	
	//Store tasks read from flat file
	@Override
	public Tasks addTask(Tasks t) {
		int rows;
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement(
				"INSERT INTO tasks(	task_name,owner_name,uat_date,task_status,r_id) VALUES(?,?,?,?,?)");
			st.setString(1, t.getTask_name());
			st.setString(2, t.getOwner());
			st.setDate(3, t.getUat_date());
			st.setString(4, t.getStatus());
			st.setInt(5, t.getTableid());
			rows = st.executeUpdate();
		}
		catch(SQLException ex){
			LOGGER.error("Error Message: " +ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " +ex.getMessage());
				}
			}
		}
		return t;
	}

	//Where daily updates will be stored
	@Override
	public String updateSpace(String update, int jira) {
		int rows;
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement(
					"UPDATE tasks set update_status = ? WHERE JiraNumber = ?");
			st.setString(1, update);
			st.setInt(2,jira);
			rows = st.executeUpdate();

		}
		catch(SQLException ex){
			LOGGER.error("Error Message: " +ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " +ex.getMessage());
				}
			}
		}
		return update;
	}

	//Add a mew member
	@Override
	public Users newMember(Users user) {
		int rows;
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement(
				"INSERT INTO Users(name,email,password,role,SecondaryScrum,isScrumMaster) VALUES(?,?,?,?,?,?)");
			st.setString(1, user.getName());
			st.setString(2, user.getEmail());
			st.setString(3, user.getPassword());
			st.setString(4, user.getRole());
			st.setBoolean(5, user.getSecondScrummaster());
			st.setBoolean(6, user.getIsScrumMaster());
			rows = st.executeUpdate();
		}
		catch(SQLException ex){
			LOGGER.error("Error Message: " +ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " +ex.getMessage());
				}
			}
		}
		return user;
	}

	//JDBC to delete a member
	@Override
	public int deleteMember(int soeid) {
		int rows;
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement(
				"DELETE FROM Users WHERE SOEID = ?");
			st.setInt(1, soeid);
			rows = st.executeUpdate();
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " +ex.getMessage());
				}
			}
		}
		return soeid;
	}

	//Update Task status
	@Override
	public String updateTaskStatus(String taskstatus, int jira) {
		int rows;
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement(
					"UPDATE tasks set task_status = ? WHERE JiraNumber = ?");
			st.setString(1, taskstatus);
			st.setInt(2,jira);
			rows = st.executeUpdate();

		}
		catch(SQLException ex){
			LOGGER.error("Error Message: " +ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " +ex.getMessage());
				}
			}
		}
		return taskstatus;
	}

	//Change Password
	@Override
	public String changePassowrd(ChangePassword changepassword) {
		int rows;
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement(
					"UPDATE users set password = ? WHERE soeid = ? AND password = ?");
			st.setString(1, changepassword.getNew_password());
			st.setInt(2,changepassword.getSoeid());
			st.setString(3, changepassword.getOld_password());
			rows = st.executeUpdate();

		}
		catch(SQLException ex){
			LOGGER.error("Error Message: " +ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " +ex.getMessage());
				}
			}
		}
		return "Success";
	}
	
	//Toggle SSM


	@Override
	public String changeSMRole(ToggleSM toogleScrumMaster) {
		int rows;
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement(
					"UPDATE users set SecondaryScrum = ? WHERE soeid = ? ");
			st.setBoolean(1, toogleScrumMaster.getSsm());
			st.setInt(2, toogleScrumMaster.getSoeid());
			rows = st.executeUpdate();

		}
		catch(SQLException ex){
			LOGGER.error("Error Message: " +ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " +ex.getMessage());
				}
			}
		}
		return "Success";
	}

	//Insert new Releases
	@Override
	public Releases newRelease(Releases release) {
		int rows;
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement(
				"INSERT INTO Releases(r_date,r_anchor,r_status) VALUES (?,?,?)");
			st.setDate(1, release.getDate());
			st.setString(2, release.getAnchor());
			st.setString(3, release.getStatus());
			rows = st.executeUpdate();
		}
		catch(SQLException ex){
			LOGGER.error("Error Message: " +ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " +ex.getMessage());
				}
			}
		}
		return release;
	}

	
	//Update Release status
	@Override
	public ReleaseStatus updateReleaseStatus(ReleaseStatus taskStatusUpdate) {
		int rows;
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement(
					"UPDATE Releases set r_status = ? WHERE tableid = ?");
			st.setString(1,taskStatusUpdate.getStatus() );
			st.setInt(2,taskStatusUpdate.getTableid());
			rows = st.executeUpdate();

		}
		catch(SQLException ex){
			LOGGER.error("Error Message: " +ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " +ex.getMessage());
				}
			}
		}
		return taskStatusUpdate;
	}

	
	@Override
	public List<Meeting> meeting() {
		List<Meeting> meetings = new ArrayList<>();
		Connection cn = null;
		ResultSet rs = null;
		
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement("SELECT * FROM Meeting");
			rs = st.executeQuery();
			while(rs.next()) {
				meetings.add(new Meeting(rs.getInt(1),rs.getBoolean(2),rs.getString(3),rs.getString(4),rs.getInt(5)));
			}	
		}
		catch(SQLException ex){
			LOGGER.error("Error Message: " +ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					LOGGER.error("Error Message: " +ex.getMessage());
				}
			}
		}
		return meetings;
	}

	@Override
	public NewUser changeName(NewUser changeName) {
		int rows;
		Connection cn = null;
		ResultSet rs = null;
		try{
			Driver d = new com.mysql.cj.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
			PreparedStatement st = cn.prepareStatement(
					"UPDATE Users set name = ? WHERE SOEID = ?");
			st.setString(1,changeName.getName() );
			st.setInt(2,changeName.getSoeid());
			rows = st.executeUpdate();

		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return changeName;
	}

	

}
